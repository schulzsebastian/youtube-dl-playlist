#!/bin/bash

if [ "$EXTENSION" == "MP3" ]; then
    yt-dlp --ignore-errors -f "bestaudio/best" --extract-audio --audio-format mp3 --audio-quality 0 --output "/playlists/$PLAYLIST/%(title)s.%(ext)s" --yes-playlist "https://youtube.com/playlist?list=$PLAYLIST"
elif [ "$EXTENSION" == "FLAC" ]; then
    yt-dlp --ignore-errors -f "bestaudio/best" --extract-audio --audio-format flac --output "/playlists/$PLAYLIST/%(title)s.%(ext)s" --yes-playlist "https://youtube.com/playlist?list=$PLAYLIST"
else
    echo "invalid extension"
fi
