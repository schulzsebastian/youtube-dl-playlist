FROM python:3.11
RUN apt-get update && apt-get install ffmpeg -y
RUN python3 -m pip install --force-reinstall https://github.com/yt-dlp/yt-dlp/archive/master.tar.gz
COPY script.sh /tmp/script.sh
CMD bash /tmp/script.sh